# Quizzes voor BEP2
Maqui is een static site generator voor quizzes.

De quizzes worden gegenereerd op basis van de .toml-bestanden en
de resulturende html-bestanden worden gekopieerd naar Gitlab Pages.
Zie daarvoor `.gitlab-ci.yml`.

Het idee is om de quizzes te embedden op de relevante Canvas-pagina (middels iframes).
Per onderwerp of les kan dan een .toml-bestand gemaakt worden.

## Lokaal uitproberen
Je kan dit lokaal uitproberen door het volgende commando:
```
    maqui build src --overview --title "Backend Programming 2"
```

Je hebt dan wel een installatie van node.js nodig en de maqui dependency.
Node.js kan je handig installeren via de 
[officiële site](https://nodejs.org/en/download/)
of via [node version manager (nvm)](https://github.com/coreybutler/nvm-windows).

Maqui kan je als (global) dependency installeren met npm:
```
    npm install -g maqui
```

Als alternatief kan je natuurlijk ook Docker gebruiken,
maar er zijn nog geen kant en klare maqui images.
